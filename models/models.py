# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Contactaddress(models.Model):
    _inherit = 'res.partner'

    # parent address fields
    Fathers_name = fields.Char(string='Fathers name')
    Fathers_phone = fields.Integer()
    Mothers_name = fields.Char(string='Mothers name')
    Mothers_phone = fields.Integer()

    # other address fields
    shop_center = fields.Char(string='Nearest shopping center')
    bus_station = fields.Char(string='Nearest bust stage')
    school = fields.Char(string='Nearest school')
    church = fields.Char(string='Nearest church')

    # # home address fields
    Phone_number = fields.Integer()
    Email_address = fields.Char(string='Email Address')
    Village = fields.Char(string='Village')
    Location = fields.Char(string='Location')
    Sub_location=fields.Char(string='Sub Location')
    Chief = fields.Char(string='Chief')
    Sub_chief = fields.Char(string='Sub Chief')

     # # Form CS fields
    Name = fields.Char()
    ID_number = fields.Integer()
    Designation = fields.Char(string='Designation')
    Grade = fields.Char(string='Grade')
    Payroll_Number=fields.Integer()
    Gross_salary = fields.Integer()
    Net_salary = fields.Integer()
    Employer=fields.Char(string='Employer')
    Department = fields.Char(string='Department')
    Section = fields.Char(string='Section')
    Station=fields.Char(string='Station')
    Town= fields.Char(string='Town')
    Total_amount= fields.Integer()

    # Employers Address
    Employerss_number = fields.Integer()
    Present_employer = fields.Char(string='Employer name')
    Occupation = fields.Char(string='Occupation')

    type = fields.Selection(
        selection=[('contact', 'Contact'),('parent', 'Parent Address'), ('home', 'Home Address'), ('employer', 'Employers Address'),
                   ('other_residence', 'Other Residence'), ('form_cs', 'Form CS'), ('land', 'Purchased Land')],
        string='Address Type', default='contact')